import {LitElement, html} from '@polymer/lit-element';
// import {css} from 'my-element-style';

class TodoView extends LitElement {

  static get properties(){
    return {
      question: {type: Array},
      valueInpit : {type: String},
      works : {type: Array},
      status: { type: Boolean},
      classDisc: { type: String},
      classInput: { type: String}
    }
  }

  constructor(){
    super();
    this.works = [
      {
        task: "Автореферат",
        task_id: 1
      },
      {
        task: "Эссе",
        task_id: 2
      },
      {
        task: "Контрольная работа",
        task_id: 3
      },
      {
        task: "Дипломная работа",
        task_id: 4
      }
    ];

    this.question = [];
    this.valueInpit = '';
    this.status = true;
    this.classDisc = '';
    this.classInput= '';
  }

    render() {
      return html`
      <style>

        .component {
          max-width: 340px;
          box-shadow: 0px 2px 16px rgba(0, 0, 0, 0.2);
          position: fixed;
          right: 50px;
          bottom: 0px;
        }
        
        .component .header {
            height: 56px;
            background-color: #4C8BF5;
            color: #fff;
            display: flex;
            align-items: center;
            padding: 0 18px;
          }
        
        .component .header__icon-message {
                width: 20px;
                height: 20px;
                background-image: url("../src/img/icon-message.svg");
                background-repeat: no-repeat;
                background-position: center;
                margin-right: 18px;
                position: relative;
              }
        
        .component .header__icon-close {
                width: 16px;
                height: 16px;
                background-image: url("../src/img/icon-close.svg");
                background-repeat: no-repeat;
                background-position: center;
                margin-left: auto;
                cursor: pointer;
              }
        
        .component .header__icon-status {
                height: 10px;
                width: 10px;
                background-color: #38C05D;
                border: 2px solid #4C8BF5;
                border-radius: 50%;
                position: absolute;
                right: -2px;
                bottom: -2px;
              }
        
        .component .header__title {
              font-weight: normal;
              font-size: 1rem;
              line-height: 1.1875rem;
            }
        
        .component .main {
            height: 400px;
            padding-left: 15px;
            display: flex;
            align-items: flex-end;
            background-color: #fff;
            transition: all .3s;
          }
        
        .component .main__content {
              overflow-x: hidden;
              overflow-y: auto;
              height: 100%;
              display: flex;
              flex-direction: column;
        
              /* &:hover {
                overflow-y: auto;
              } */
            }
        
        .component .main .item {
              display: flex;
              padding-right: 10px;
        
            }
        
        .component .main .item-im {
                align-self: flex-end;
                text-align: right;
              }
        
        .component .main .item__img {
                margin-right: 15px;
              }
        
        .component .main .item__img img{
                  border-radius: 50%;
                  width: 40px;
                  height: 40px;
                }
        
        .component .main .item__name {
                color: #263238;
                font-size: 0.75rem;
                line-height: 0.9375rem;
                font-weight: bold;
                margin-bottom: 5px;
              }
        
        .component .main .item__text {
                color: #263238;
                font-size: 0.875rem;
                line-height: 1.0625rem;
              }
        
        .component .main .item__bg {
                background: #f6f2f2;
                border-radius: 8px;
                padding: 12px 15px;
                margin-bottom: 12px;
              }
        
        .component .main .item__form {
                margin-top: 8px;
              }
        
        .component .main .item__quest {
                width: 220px;
                height: 40px;
                margin-bottom: 8px;
                border: 1px solid #E0E0E0;
                box-sizing: border-box;
                border-radius: 4px;
                outline: none;
              }
        
        .component .main .item__quest-thing{
                  font-size: 0.875rem;
                  line-height: 1.0625rem;
                  padding: 0 12px;
                }
        
        .component .main .item__quest-thing::-webkit-input-placeholder {
                  color: #C0C3C5;
                }
        
        .component .main .item__quest-thing::-moz-placeholder {
                  color: #C0C3C5;
                }
        
        .component .main .item__quest-thing:-ms-input-placeholder {
                  color: #C0C3C5;
                }
        
        .component .main .item__quest-thing::-ms-input-placeholder {
                  color: #C0C3C5;
                }
        
        .component .main .item__quest-thing::placeholder {
                  color: #C0C3C5;
                  
                }
        
        .component .main .item__quest-btn {
                  background: #38C05D;
                  border-radius: 4px;
                  border: none;
                  color: #fff;
                  font-size: 0.875rem;
                  line-height: 1.0625rem;
                  cursor: pointer;
                  transition: all 0.3s;

                }
        .component .main .item__quest-btn:hover {
            background: #2c9649;
            transition: all 0.3s;
        }
        .component .main .item__date {
                color: #C0C3C5;
                font-size: 0.75rem;
                line-height: 0.9375rem;
                margin-bottom: 20px;
              }
              .item__hint {
                font-size: 11px;
                color:#EC533E;
                display: none;
              }
              .item__hint-show {
                display: block;
              }

              .main-hide {
                height: 0px !important;
                transition: all .3s;
              }
      </style>
      
    
      <hr>
      <div class="component">
      <div class="component__header header">
         <div class="header__icon-message"><span class="header__icon-status"></span></div>
         <div class="header__title">Задайте нам вопрос</div>
         <div class="header__icon-close"
          @click = " ${ this.closeModal }"
         ></div>
      </div>
      <div class="component__main main ${ this.classDisc }">
        <div class="main__content">

          <div class="main__item item">
            <div class="item__img">
              <img src="../src/img/img-chat.svg" alt="img">
            </div>
            <div class="item__disc">
              <div class="item__name">Ксения</div>
              <div class="item__bg">
                <span class="item__text">Здравствуйте!</span>
              </div>
              <div class="item__bg">
                <span class="item__text">По какому предмету вам нужна помощь?</span>
                <form action="" class="item__form">
                  <input type="text" placeholder="Ваш ответ ..." class="item__quest item__quest-thing" 
                    .value = "${this.valueInpit}"
                    @change = " ${this.valueWrite}"
                  >
                  <div class="item__hint ${ this.classInput }">
                  <img src="../src/img/error.svg" alt="img">
                  По какому предмету вам нужна помощь?</div>
                  <input type="submit" value="Отправить" class="item__quest item__quest-btn"
                    @click = "${this.addAnswer}"
                  >
                </form>
              </div>
              <div class="item__date">
                  Меньше минуты назад
                  ${this.valueInpit}
              </div>
            </div>
          </div>


          <div class="main__item item item-im">

            <div class="item__disc">
              <div class="item__name">Вы</div>

            ${
               this.question.map(item => html`
                <div class="item__bg">
                  <span class="item__text">${item.quest}</span>
                </div>
                <div class="item__date">
                  ${item.date}
                </div>
               `)
            }

            </div>
          </div>


          <div class="main__item item">
              <div class="item__img">
                <img src="../src/img/img-chat.svg" alt="img">
              </div>
              <div class="item__disc">
                <div class="item__name">Ксения</div>
                <div class="item__bg">
                  <span class="item__text">Подскажите. пожалуйста, тип работы:</span>
                  <form action="" class="item__form">
                    <select placeholder="Курсовая работа" class="item__quest item__quest-thing" required>
                      <option selected disabled hidden>Курсовая работа</option>
                      ${
                        this.works.map(task => html`
                        <option .value="${ task.task_id}"> ${ task.task} </option>
                        `)
                      }
                    </select>
                    <input type="submit" value="Отправить" class="item__quest item__quest-btn">
                  </form>

                </div>
                <div class="item__date">
                    Меньше минуты назад
                </div>
              </div>
            </div>



        </div>
      </div>
  </div>
      `;
    }

    addAnswer(e){
      e.preventDefault();
      if (this.valueInpit) {
   
         let hours = new Date().getHours();
         let minute = new Date().getMinutes();
         let date = hours+':'+minute;

        this.question = [...this.question, 
          { quest: this.valueInpit,
            date:  date
          }
        ]

        console.log("Question complited");
        console.log(this.question);
        this.valueInpit = '';
        this.classInput = "";
      } else {
        console.log("Err!");
        this.classInput = "item__hint-show";
      }
    }

    valueWrite(e) {
      this.valueInpit = e.target.value;
    }

    closeModal(){
      this.status = !this.status;
      if (this.status ) {
        this.classDisc = "";
      } else {
        this.classDisc = "main-hide"
      }
    }

    addTask() {
      if (this.task){
        this.todos = [...this.todos , {
          task : this.task,
          complete: false
        }];
        this.task = '';
      }
    }

    deleteTask(todo){
      this.todos = this.todos.filter(el => el !== todo)
    }

    updateCheck(newTodo, completed){
      this.todos = this.todos.map(todo => 
        newTodo === todo ? {...newTodo, completed } : todo
      )
    }
    
}

customElements.define('todo-view',TodoView)
