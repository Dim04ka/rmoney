import {html} from 'lit-element';

export const css = html`
    .section-btn__button { 
      display: block;
    }
    .section-btn__button-hide {
      display: none;
    }
`;